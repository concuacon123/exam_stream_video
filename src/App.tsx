import React from 'react';
import {View, StyleSheet} from 'react-native';
import Video from 'react-native-video';

const App = () => (
  <View style={styles.container}>
    <Video
      source={{
        uri: 'https://utopmspoc-aase.streaming.media.azure.net/991cfbbd-cb8b-4b1e-8d58-7b26f8a2fa77/test.ism/manifest(format=m3u8-aapl)',
        type: 'm3u8',
      }}
      resizeMode="cover"
      style={styles.backgroundVideo}
    />
  </View>
);

export default App;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backgroundVideo: {
    width: 400,
    height: 300,
  },
});
